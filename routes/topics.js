var express = require('express');
var router = express.Router();

var authentication = require('../utils/authentication');
var Models = require('../models');

// Huom! Kaikki polut alkavat polulla /topics

// GET /topics
router.get('/', function(req, res, next) {
    // Hae kaikki aihealueet tässä (Vinkki: findAll)
   Models.Topic.all().then(topics => {
    res.send(topics);
  })
});

// GET /topics/:id
router.get('/:id', function(req, res, next) {
  // Hae aihealue tällä id:llä tässä (Vinkki: findOne)
 Models.Topic.findById(req.params.id, { include: { model: Models.Message, include: { model: Models.User, attributes: ['username']}}} ).then(topic => {
    res.send(topic);
  })
});

// POST /topics
router.post('/',authentication, function(req, res, next) {
  // Lisää tämä aihealue
   Models.Topic.create({ 
    name: req.body.name, 
    description: req.body.description
  })

  // Palauta vastauksena lisätty aihealue
  .then(newTopic =>  {
    res.send(newTopic)
  })
});


// POST /topics/:id/message
router.post('/:id/message', function(req, res, next) {
  // Lisää tällä id:llä varustettuun aihealueeseen...
  // ...tämä viesti (Vinkki: lisää ensin messageToAdd-objektiin kenttä TopicId, jonka arvo on topicId-muuttujan arvo ja käytä sen jälkeen create-funktiota)
   Models.Message.create({ 
    title: req.body.title, 
    content: req.body.content,
    TopicId: req.params.id,
    UserId: req.session.userId
  })
  // Palauta vastauksena lisätty viesti
  .then(message =>  {
    res.send(message)
  })

});

module.exports = router;
