var express = require('express');
var router = express.Router();
var bcrypt = require('bcrypt');
var Models = require('../models');

// Huom! Kaikki polut alkavat polulla /users

// POST /users
router.post('/', function(req, res, next){
  // Lisää tämä käyttäjä (Vinkki: create), muista kuitenkin sitä ennen varmistaa, että käyttäjänimi ei ole jo käytössä! (Vinkki: findOne)
  var userToAdd = req.body;
 if (!req.body.username || !req.body.password)
    res.status(400).json({ error: 'Käyttäjätunnus tai salasana puutuu!' })
  Models.User.findOne({  
    where: { username: userToAdd.username } })
  .then(function(user){ 
    if (user)
      res.status(400).json({ error: 'Käyttäjätunnus on jo käytössä!' })
    else {
      if (userToAdd.password.length > 6) {
        bcrypt.hash(userToAdd.password, 10, function(err, hash) {
          if (err) {
            res.send(err)
            return
          }
          Models.User.create({ 
            username: userToAdd.username, 
            password: hash
          })
          .then(newUser =>  {
            req.session.userId = newUser.id;
            res.send(newUser)
          })
        });

      }
      else res.status(400).json({ error: 'Salasanan on oltava yli 6 merkkiä pitkä.' })
    }
  })

});

// POST /users/authenticate
router.post('/authenticate', function(req, res, next){
  // Tarkista käyttäjän kirjautuminen tässä. Tee se katsomalla, löytyykö käyttäjää annetulla käyttäjätunnuksella ja salasanalla (Vinkki: findOne ja sopiva where)
 var userToCheck = req.body;
  if (userToCheck == null || userToCheck.username == null || userToCheck.password == null)
    res.status(403).json({ error: 'Puuttuvia kenttiä.' })
  Models.User.findOne({
    where: {username: userToCheck.username}})
  .then(function(user){    
    if(user){
      bcrypt.compare(userToCheck.password, user.password, function(err, result) {
        if (err) {
          res.send(err)
          return
        }
        if(result) {
          req.session.userId = user.id;
          user.password = undefined;
          res.send(user)
        } else res.status(403).json({ error: 'Väärä salasana.' })
      });
    } else res.status(403).json({ error: 'Käyttäjätunnus ei ole olemassa.' })
  }); 
});


// GET /users/logged-in
router.get('/logged-in', function(req, res, next){
  var loggedInId = req.session.userId ? req.session.userId : null;

  if(loggedInId == null){
    res.json({error: 'Et ole kirjautunut sisään'});
  }else{
    // Hae käyttäjä loggedInId-muuttujan arvon perusteella (Vinkki: findOne)
   Models.User.findById(loggedInId).then(user => {
      res.send(user)
    })
  }
});

// GET /users/logout
router.get('/logout', function(req, res, next){
  req.session.userId = null;

  res.send(200);
});

module.exports = router;
