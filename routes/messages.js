var express = require('express');
var router = express.Router();


var Models = require('../models');

// Huom! Kaikki polut alkavat polulla /messages

// GET /messages/:id
router.get('/:id', function(req, res, next) {
  // Hae viesti tällä id:llä ja siihen liittyvät vastaukset tässä (Vinkki: findOne ja sopiva include)
 Models.Message.findById(req.params.id,
 { include: { model: Models.Reply, include: 
 { model: Models.User, attributes: ['username']}}} )
 .then(message => {
  res.send(message);
  })
});

// POST /messages/:id/reply
router.post('/:id/reply', function(req, res, next){
  // Lisää tällä id:llä varustettuun viestiin...
  // ...tämä vastaus (Vinkki: lisää ensin replyToAdd-objektiin kenttä MessageId, jonka arvo on messageId-muuttujan arvo ja käytä sen jälkeen create-funktiota)
   Models.Reply.create({ 
    content: req.body.content,
    MessageId: req.params.id,
    UserId: req.session.userId
  })
  // Palauta vastauksena lisätty vastaus
 .then(reply =>  {
    res.send(reply)
  })
});


module.exports = router;
