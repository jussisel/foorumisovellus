FoorumApp.controller('ShowMessageController', function($scope, $routeParams,$rootScope, Api){
  // Toteuta kontrolleri tähän
   Api.getMessage($routeParams.id).success(function(message){
        $scope.message = message;
        $scope.newReply = {
            content: ''
        };
        console.log(message)
        $scope.replyCount = message.Replies.length;
    })
    .error(function(data, status, headers, config){
      console.log('Jotain meni pieleen...');
    })
    
    $scope.addReply = function(reply, id) {
        if (reply.content) {
            Api.addReply(reply, id).success(function(reply){
                reply['User'] = {username: $rootScope.userLoggedIn.username}
                $scope.message.Replies.push(reply);
                $scope.newReply = {
                    content: ''
                }
                $scope.replyCount++
            })
            .error(function(data, status, headers, config){
                console.log('Virhe!!');
            })           
        }
        else alert('Viesti virheellinen.')
    }

});
