FoorumApp.controller('ShowTopicController', function($scope, $routeParams, $location,$rootScope, Api){
  // Toteuta kontrolleri tähän
  Api.getTopic($routeParams.id).success(function(topic){
        $scope.topic = topic;
        $scope.newMessage = {
            title: '',
            content: ''
        }
        $scope.messageCount = topic.Messages.length;
    })
    .error(function(data, status, headers, config){
      console.log('Jotain meni pieleen...');
    })
    
    $scope.addMessage = function(message, id) {
      if (message.title && message.content) {
        Api.addMessage(message, id).success(function(message){
          message['User'] = {username: $rootScope.userLoggedIn.username}
          $scope.topic.Messages.push(message);
          $scope.newMessage = {
            title: '',
            content: ''
          }
          $scope.messageCount++
        })
        .error(function(data, status, headers, config){
          console.log('Virhe!!');
        })
      }
      else alert('viesti virheellinen')
    }

});
