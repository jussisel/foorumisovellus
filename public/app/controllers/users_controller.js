FoorumApp.controller('UsersController', function($scope, $location, Api){
  // Toteuta kontrolleri tähän
   $scope.user = {
        username: '',
        password: ''
    }
    
    
    $scope.passwordConfirm = '';
    
    
    //kirjautuminen
    $scope.login = function(user) {
        Api.login(user).success(function(user){    
            console.log('Kirjautuminen hyväksytty');
            $location.path('/')  
        })  
        
        .error(function(res){
            $scope.errorMessage = res.error;  
        });       
    }
    
    
    // tarkistaa salasanan
    $scope.register = function(user) {
        if ($scope.passwordConfirm != user.password) {
            $scope.errorMessage = "Salasanat ovat erilaiset."
            return
        }
        
        Api.register(user).success(function(user){    
            console.log('Kirjautuminen hyväksytty');
            $location.path('/')  
        })  
        
        .error(function(res){
            $scope.errorMessage = res.error;  
        });    
    }

});
